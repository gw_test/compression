package com.gateweb.compression;

import com.gateweb.compression.entity.InvoiceMainEntity;
import com.gateweb.compression.model.InvoiceMain;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface InvoiceMapper {

  InvoiceMapper INSTANCE = Mappers.getMapper(InvoiceMapper.class);

  @Mapping(source = "seller", target = "sellerIdentifier")
  @Mapping(source = "buyer", target = "buyerIdentifier")
  @Mapping(
      target = "taxAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getTaxAmount()).getDouble())")
  @Mapping(
      target = "discountAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getDiscountAmount()).getDouble())")
  @Mapping(
      target = "originalCurrencyAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getOriginalCurrencyAmount()).getDouble())")
  @Mapping(
      target = "freeTaxSalesAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getFreeTaxSalesAmount()).getDouble())")
  @Mapping(
      target = "salesAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getSalesAmount()).getDouble())")
  @Mapping(
      target = "totalAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getTotalAmount()).getDouble())")
  @Mapping(
      target = "zeroTaxSalesAmount",
      expression =
          "java( com.gateweb.compression.Converters.convert1(invoiceMainEntity.getZeroTaxSalesAmount()).getDouble())")
  @Mapping(
      target = "details",
      expression = "java(new java.util.ArrayList<com.gateweb.compression.model.InvoiceDetail>())")
  InvoiceMain invoiceMainEntityToInvoiceMain(InvoiceMainEntity invoiceMainEntity);
}
