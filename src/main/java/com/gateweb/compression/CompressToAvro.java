package com.gateweb.compression;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.fasterxml.jackson.dataformat.avro.deser.AvroFieldReader;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.gateweb.compression.model.Test;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.avro.Schema;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CompressToAvro {

  private static Logger logger = LoggerFactory.getLogger(CompressToAvro.class);
  private ObjectMapper objectMapper;
  private XmlMapper xmlMapper;
  //  private AvroMapper avroMapper;

  public CompressToAvro(ObjectMapper objectMapper, XmlMapper xmlMapper) {
    this.objectMapper = objectMapper;
    this.xmlMapper = xmlMapper;
  }

  public <T> void toAvro(Class<T> format, String root, String sourceFolder, String targetFolder)
      throws IOException, CsvException, IllegalAccessException, InstantiationException, InvocationTargetException {
    final Map<Path, Path> allFiles =
        findAllFiles(Paths.get(root), sourceFolder, targetFolder, ".avro");
    toAvro(format, allFiles);
  }

  public <T> void toAvro(Class<T> format, Map<Path, Path> allFiles)
      throws IOException, CsvException, IllegalAccessException, InvocationTargetException, InstantiationException {
    for (Path sourceFile : allFiles.keySet()) {
      final Path targetFile = allFiles.get(sourceFile);
      compress(format, sourceFile, targetFile);
    }
  }

  public <T > void fromAvro(Class<T> format, Map<Path, Path> allFiles) throws IOException {
    for (Path sourceFile : allFiles.keySet()) {
      final Path targetFile = allFiles.get(sourceFile);
      uncompress(format, sourceFile, targetFile);
    }
  }

  public <T> void fromAvro(
      Class<T> format,
      String root,
      String sourceFolder,
      String targetFolder,
      String targetExtension)
      throws IOException {
    final Map<Path, Path> allFiles =
        findAllFiles(Paths.get(root), sourceFolder, targetFolder, targetExtension);
    fromAvro(format, allFiles);
  }

  // 找出該目錄下所有檔案路徑
  private Map<Path, Path> findAllFiles(
      Path root, String sourceFolder, String targetFolder, String targetExtension)
      throws IOException {
    final Map<Path, Path> map = new LinkedHashMap<>();
    final Path source = root.resolve(sourceFolder);
    try (Stream<Path> stream = Files.walk(source).filter(Files::isRegularFile)) {
      stream.forEach(
          sourceFile -> {
            final String targetStr =
                StringUtils.substringBeforeLast(
                        sourceFile
                            .toString()
                            .replace(
                                File.separatorChar + sourceFolder,
                                File.separatorChar + targetFolder),
                        ".")
                    + targetExtension;
            final Path targetFile = Paths.get(targetStr);
            if (Files.notExists(targetFile)) {
              try {
                Files.createDirectories(targetFile.getParent());
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
            map.put(sourceFile, targetFile);
          });
      return map;
    }
  }

  private <T> void compress(Class<T> format, Path sourceFile, Path targetFile)
      throws IOException, CsvException, IllegalAccessException, InstantiationException, InvocationTargetException {
    final String extension = com.google.common.io.Files.getFileExtension(sourceFile.toString());
    List<T> tests = new ArrayList<>();
    switch (extension) {
      case "json":

        break;
      case "xml":
        break;
      case "csv":
        // read csv
        try (CSVReader reader = new CSVReader(new FileReader(sourceFile.toFile()))) {
          // headers
          final String[] headers = reader.readNext();
          final String[] fields = StringUtils.split(headers[0], ",");

          final T t = format.newInstance();
          final List<T> objs = new ArrayList<>();
          final List<String[]> contents = reader.readAll();
          for (String[] row : contents) {
            Map<String,Object> map = new HashMap();
            for (int i = 0; i < fields.length; i++) {
              final String field = fields[i];
              final String value = row[i];
//              ReflectUtils.invokeSetter(t, field, value);
                            map.put(field,value);
              // TODO or map?
            }
             BeanUtils.populate(t,map);
            objs.add(t);
          }
//          tests =
//              reader.readAll().stream()
//                  .map(
//                      row -> {
//                        try {
//                          final T t = format.newInstance();
//                          Map<String,Object> map = new HashMap();
//                          for (int i = 0; i < fields.length; i++) {
//                            final String field = fields[i];
//                            final String value = row[i];
//                          ReflectUtils.invokeSetter(t, field, value);
////                            map.put(field,value);
//                            // TODO or map?
//                          }
////                          BeanUtils.populate(t,map);
//                          return t;
//                        } catch (InstantiationException | IllegalAccessException e) {
//                          e.printStackTrace();
//                          return null;
//                        }
//                      })
//                  .collect(Collectors.toList());
        }
        break;
    }
    if (tests.isEmpty()) {
      logger.error("no data");
      return;
    }

    // compress to avro
    final DatumWriter<T> datumWriter = new SpecificDatumWriter<>(format);
    try (DataFileWriter dataFileWriter = new DataFileWriter(datumWriter)) {
      dataFileWriter.setCodec(CodecFactory.snappyCodec());
      dataFileWriter.create(Test.SCHEMA$, targetFile.toFile());
      for (T test : tests) {
        dataFileWriter.append(test);
      }
    }
    logger.info("[ToAvro OK] => {} to {}", sourceFile, targetFile);
  }

  private <T> void uncompress(Class<T> format, Path sourceFile, Path targetFile)
      throws IOException {
    // read avsc
    final List<T> raws = new ArrayList<>();

    final DatumReader<T> datumReader = new SpecificDatumReader<>(format);
    try (DataFileReader<T> dataFileReader = new DataFileReader(sourceFile.toFile(), datumReader)) {
      dataFileReader.previousSync();
      while (dataFileReader.hasNext()) {
        final T obj = dataFileReader.next();
        raws.add(obj);
        logger.info("{}", obj);
      }
    }
    final String extension = com.google.common.io.Files.getFileExtension(targetFile.toString());

    // write
    switch (extension) {
      case "json":
        // TODO
        break;
      case "xml":
        // TODO
        avroToXml(raws, targetFile);
        break;
      case "csv":
        avroToCsv(raws, targetFile);
        break;
    }

    logger.info("[FromAvro OK] => {} to {}", sourceFile, targetFile);
  }

  private <T> void avroToXml(List<T> raws, Path targetFile) throws IOException {
    System.out.println("avro:");
    Map map = new HashMap();
    for (T t : raws) {
      //      Test test = (Test) t;
      String[] fields = ReflectUtils.getPrivateFieldsWithoutStaticAndFinal(t);

      for (String field : fields) {
        map.put(field, ReflectUtils.invokeGetter(t, field));
        logger.info("{} : {}", field, ReflectUtils.invokeGetter(t, field));
      }
      // key name -> field name
      //      map.put(fields[0].toString(), test.getHostName());
      //      map.put(fields[1].toString(), test.getIpAddress());
      //      System.out.println(fields[0].toString() + " " + fields[1].toString());
    }

    // 序列化時加上文件頭信息
    xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
    xmlMapper.writerWithDefaultPrettyPrinter().writeValue(targetFile.toFile(), map);
  }

  private <T> void avroToCsv(List<T> raws, Path targetFile) throws IOException {
    //        final List<String[]> csvRaws = new ArrayList<>();
    final String[] fields = ReflectUtils.getPrivateFieldsWithoutStaticAndFinal(raws.get(0));
    logger.info("hearder: {}", StringUtils.join(fields, ","));

    // to String[]
    final List<String[]> csvRaws =
        raws.stream()
            .map(
                raw ->
                    Arrays.stream(fields)
                        .map(field -> ReflectUtils.invokeGetter(raw, field))
                        .toArray(String[]::new))
            .collect(Collectors.toList());

    try (CSVWriter csvWriter = new CSVWriter(new FileWriter(targetFile.toFile()))) {
      csvWriter.writeNext(fields);
      csvWriter.writeAll(csvRaws);
    }
  }

  abstract class IgnoreSchemaProperty {
    // You have to use the correct package for JsonIgnore,
    // fasterxml or codehaus
    @JsonIgnore
    abstract void getSchema();
  }
}
