package com.gateweb.compression;

import com.gateweb.compression.model.InvoiceDetail;
import com.gateweb.compression.model.InvoiceMain;
import org.apache.avro.Conversions;
import org.apache.avro.LogicalTypes;

import java.math.BigDecimal;
import java.nio.ByteBuffer;

public class Converters {

  private static final Conversions.DecimalConversion decimalConversion = new Conversions.DecimalConversion();

  public static ByteBuffer convert1(BigDecimal bigDecimal) {
    if (bigDecimal == null)
      return decimalConversion.toBytes(BigDecimal.ZERO, InvoiceMain.SCHEMA$, LogicalTypes.decimal(53, 4));
    return decimalConversion.toBytes(bigDecimal, InvoiceMain.SCHEMA$, LogicalTypes.decimal(53, 4));
  }

  public static ByteBuffer convert2(BigDecimal bigDecimal) {
    if (bigDecimal == null)
      return decimalConversion.toBytes(BigDecimal.ZERO, InvoiceDetail.SCHEMA$, LogicalTypes.decimal(32, 9));
    return decimalConversion.toBytes(bigDecimal, InvoiceDetail.SCHEMA$, LogicalTypes.decimal(32, 9));

  }
}
