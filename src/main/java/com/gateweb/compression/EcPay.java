package com.gateweb.compression;

import com.univocity.parsers.annotations.Parsed;

public class EcPay {


  @Parsed(index = 0)  String no;
  @Parsed(index = 1)  String invoiceNumber;
  @Parsed(index = 2)  String uniqueNumber;
  @Parsed(index = 3)  String salesAmount;
  @Parsed(index = 4)  String taxAmount;
  @Parsed(index = 5)  String discountAmount;
  @Parsed(index = 6)  String randomNumber;
  @Parsed(index = 7)  String invoiceDateTime;
  @Parsed(index = 8)  String uploadStatus;
  @Parsed(index = 9)  String printMark;
  @Parsed(index = 10)  String prize;
  @Parsed(index = 11)  String modify;
  @Parsed(index = 12)  String customerNumber;
  @Parsed(index = 13)  String buyerIdentifier;
  @Parsed(index = 14)  String buyerName;
  @Parsed(index = 15)  String buyerAddress;
  @Parsed(index = 16)  String buyerPhoneNumber;
  @Parsed(index = 17)  String buyerEmailAddress;
  @Parsed(index = 18)  String mainRemark;
  @Parsed(index = 19)  String taxType;
  @Parsed(index = 20)  String c;
  @Parsed(index = 21)  String carrierType;
  @Parsed(index = 22)  String carrierId;
  @Parsed(index = 23)  String npoban;
  @Parsed(index = 24)  String description;
  @Parsed(index = 25)  String quantity;
  @Parsed(index = 26)  String unit;
  @Parsed(index = 27)  String unitPrice;
  @Parsed(index = 28)  String amount;
  @Parsed(index = 29)  String remark;

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
  }

  public String getUniqueNumber() {
    return uniqueNumber;
  }

  public void setUniqueNumber(String uniqueNumber) {
    this.uniqueNumber = uniqueNumber;
  }

  public String getSalesAmount() {
    return salesAmount;
  }

  public void setSalesAmount(String salesAmount) {
    this.salesAmount = salesAmount;
  }

  public String getTaxAmount() {
    return taxAmount;
  }

  public void setTaxAmount(String taxAmount) {
    this.taxAmount = taxAmount;
  }

  public String getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(String discountAmount) {
    this.discountAmount = discountAmount;
  }

  public String getRandomNumber() {
    return randomNumber;
  }

  public void setRandomNumber(String randomNumber) {
    this.randomNumber = randomNumber;
  }

  public String getInvoiceDateTime() {
    return invoiceDateTime;
  }

  public void setInvoiceDateTime(String invoiceDateTime) {
    this.invoiceDateTime = invoiceDateTime;
  }

  public String getUploadStatus() {
    return uploadStatus;
  }

  public void setUploadStatus(String uploadStatus) {
    this.uploadStatus = uploadStatus;
  }

  public String getPrintMark() {
    return printMark;
  }

  public void setPrintMark(String printMark) {
    this.printMark = printMark;
  }

  public String getPrize() {
    return prize;
  }

  public void setPrize(String prize) {
    this.prize = prize;
  }

  public String getModify() {
    return modify;
  }

  public void setModify(String modify) {
    this.modify = modify;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getBuyerIdentifier() {
    return buyerIdentifier;
  }

  public void setBuyerIdentifier(String buyerIdentifier) {
    this.buyerIdentifier = buyerIdentifier;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getBuyerAddress() {
    return buyerAddress;
  }

  public void setBuyerAddress(String buyerAddress) {
    this.buyerAddress = buyerAddress;
  }

  public String getBuyerPhoneNumber() {
    return buyerPhoneNumber;
  }

  public void setBuyerPhoneNumber(String buyerPhoneNumber) {
    this.buyerPhoneNumber = buyerPhoneNumber;
  }

  public String getBuyerEmailAddress() {
    return buyerEmailAddress;
  }

  public void setBuyerEmailAddress(String buyerEmailAddress) {
    this.buyerEmailAddress = buyerEmailAddress;
  }

  public String getMainRemark() {
    return mainRemark;
  }

  public void setMainRemark(String mainRemark) {
    this.mainRemark = mainRemark;
  }

  public String getTaxType() {
    return taxType;
  }

  public void setTaxType(String taxType) {
    this.taxType = taxType;
  }

  public String getC() {
    return c;
  }

  public void setC(String c) {
    this.c = c;
  }

  public String getCarrierType() {
    return carrierType;
  }

  public void setCarrierType(String carrierType) {
    this.carrierType = carrierType;
  }

  public String getCarrierId() {
    return carrierId;
  }

  public void setCarrierId(String carrierId) {
    this.carrierId = carrierId;
  }

  public String getNpoban() {
    return npoban;
  }

  public void setNpoban(String npoban) {
    this.npoban = npoban;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getQuantity() {
    return quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(String unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }
}
