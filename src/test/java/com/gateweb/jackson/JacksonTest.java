package com.gateweb.jackson;


import com.gateweb.compression.CompressToAvro;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

/**
 * @program: avro_project
 * @description:
 * @author: Allen.Liao
 * @createL 2021-02-25 14:26
 */

//@ExtendWith(SpringExtension.class)
//@SpringBootTest(classes = {CompressToAvro.class})
public class JacksonTest {

  CompressToAvro compressToAvro = new CompressToAvro();

//  @Test
//  public void whenJavaSerializedToXmlStr_thenCorrect() throws JsonProcessingException {
//    XmlMapper xmlMapper = new XmlMapper();
//    String xml = xmlMapper.writeValueAsString(new SimpleBean());
//    assertNotNull(xml);
//  }
//
//  @Test
//  public void whenJavaSerializedToXmlFile_thenCorrect() throws IOException {
//    XmlMapper xmlMapper = new XmlMapper();
//
//    //        xmlMapper.writeValue(new File("simple_bean.xml"), new SimpleBean());
//    //          File file = new File("simple_bean.xml");
//    //        assertNotNull(file);
//
//    com.gateweb.jackson.Test test = new com.gateweb.jackson.Test();
//    List<String[]> raws = new ArrayList<>();
//    raws.add(new String[] {"a", "123"});
//    xmlMapper.writeValue(new File("testXml.xml"), test);
//    File file = new File("testXml.xml");
//    assertNotNull(file);
//  }

  @Test
  public void ss() throws IOException {
    final String root = "src/test/resources";
    compressToAvro.fromAvro(root, "avro", "result", ".xml");
  }

//  @Test
//  public void avroToXml() throws IOException {
//
//    // read avsc
//    Path root = Paths.get("src/test/resources/avro");
//    final Path sourceFile = root.resolve("avro");
//    final DatumReader<com.gateweb.compression.model.Test> datumReader =
//        new SpecificDatumReader<>(com.gateweb.compression.model.Test.class);
//    try (DataFileReader<com.gateweb.compression.model.Test> dataFileReader =
//        new DataFileReader(sourceFile.toFile(), datumReader)) {
//      dataFileReader.previousSync();
//      final List<String[]> raws = new ArrayList<>();
//      while (dataFileReader.hasNext()) {
//        final com.gateweb.compression.model.Test test = dataFileReader.next();
//        raws.add(new String[] {test.getHostName(), test.getIpAddress()});
//      }
//      // write
//      String extension = "xml";
//      switch (extension) {
//        case "json":
//          // TODO
//          break;
//        case "xml":
//          // TODO
//          //                    Schema schema = new Schema.Parser().parse(new
//          // File("/path/to/emp.avsc"));
//          //                    convert(dataFileReader);
//
//          XmlMapper xmlMapper = new XmlMapper();
//          String xml = xmlMapper.writeValueAsString(new SimpleBean());
//          assertNotNull(xml);
//          break;
//        case "csv":
//          //                    try (CSVWriter csvWriter = new CSVWriter(new
//          // FileWriter(targetFile.toFile()))) {
//          //                        csvWriter.writeNext(StringUtils.split("hostName,ipAddress"));
//          //                        csvWriter.writeAll(raws);
//          //                    }
//          break;
//      }
//    }
//  }

}
